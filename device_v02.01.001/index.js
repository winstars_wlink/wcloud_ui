module.exports = (router) => {
  router.all('/', async (ctx) => {
    ctx.redirect('/error') ;
  });
  
  router.use('/v02.01.001/networkSet', require('./networkSet'))
  router.use('/v02.01.001/status', require('./status'))
  router.use('/v02.01.001/app', require('./app'))
  router.use('/v02.01.001/wifiset', require('./wifiset'))
  router.use('/v02.01.001/error', require('./error'))
  router.use('/v02.01.001/timeZone', require('./timeZone'))
  router.use('/v02.01.001/deviceManage', require('./deviceManage'))
  router.use('/v02.01.001/editClient', require('./editClient'))
  router.use('/v02.01.001/modifyPwd', require('./modifyPwd'))
  router.use('/v02.01.001/light', require('./light'))
  router.use('/v02.01.001/signalCondition', require('./signalCondition'))
  router.use('/v02.01.001/lanSet', require('./lanSet'))
  router.use('/v02.01.001/guest', require('./guest'))
  router.use('/v02.01.001/sysUpgrade', require('./sysUpgrade'))
  router.use('/v02.01.001/meshManage', require('./meshManage'))
  router.use('/v02.01.001/addMesh', require('./addMesh'))
  router.use('/v02.01.001/touchLink', require('./touchLink'))
  router.use('/v02.01.001/reboot', require('./reboot'))
}
