module.exports = (router) => {
  router.all('/', async (ctx) => {
    ctx.redirect('/error') ;
  });
  
  router.use('/v02.002/networkSet', require('./networkSet'))
  router.use('/v02.002/status', require('./status'))
  router.use('/v02.002/app', require('./app'))
  router.use('/v02.002/wifiset', require('./wifiset'))
  router.use('/v02.002/error', require('./error'))
  router.use('/v02.002/timeZone', require('./timeZone'))
  router.use('/v02.002/deviceManage', require('./deviceManage'))
  router.use('/v02.002/editClient', require('./editClient'))
  router.use('/v02.002/modifyPwd', require('./modifyPwd'))
  router.use('/v02.002/light', require('./light'))
  router.use('/v02.002/signalCondition', require('./signalCondition'))
  router.use('/v02.002/lanSet', require('./lanSet'))
  router.use('/v02.002/guest', require('./guest'))
  router.use('/v02.002/sysUpgrade', require('./sysUpgrade'))
  router.use('/v02.002/meshManage', require('./meshManage'))
  router.use('/v02.002/addMesh', require('./addMesh'))
  router.use('/v02.002/touchLink', require('./touchLink'))
  router.use('/v02.002/reboot', require('./reboot'))
}
