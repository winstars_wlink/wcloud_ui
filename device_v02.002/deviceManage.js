const Router = require('koa-router')
const router = new Router()

router.get('/', async (ctx) => {
  ctx.state.__ = ctx.__.bind(ctx);
  let locale =ctx.__getLocale();

  await ctx.render('v02.002/deviceManage', {
      pageid:"deviceManage", 
      menuid:"deviceManage", 
      viewid: "v02.002",
      build_date: config.build_date, 
      locale:locale});

});

module.exports = router.routes()