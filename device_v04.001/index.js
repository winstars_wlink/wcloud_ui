module.exports = (router) => {
  router.all('/', async (ctx) => {
    ctx.redirect('/error') ;
  });
  
  router.use('/v04.001/networkSet', require('./networkSet'))
  router.use('/v04.001/status', require('./status'))
  router.use('/v04.001/app', require('./app'))
  router.use('/v04.001/wifiset', require('./wifiset'))
  router.use('/v04.001/error', require('./error'))
  router.use('/v04.001/timeZone', require('./timeZone'))
  router.use('/v04.001/deviceManage', require('./deviceManage'))
  router.use('/v04.001/editClient', require('./editClient'))
  router.use('/v04.001/modifyPwd', require('./modifyPwd'))
  router.use('/v04.001/light', require('./light'))
  router.use('/v04.001/signalCondition', require('./signalCondition'))
  router.use('/v04.001/lanSet', require('./lanSet'))
  router.use('/v04.001/guest', require('./guest'))
  router.use('/v04.001/sysUpgrade', require('./sysUpgrade'))
  router.use('/v04.001/meshManage', require('./meshManage'))
  router.use('/v04.001/addMesh', require('./addMesh'))
  router.use('/v04.001/touchLink', require('./touchLink'))
  router.use('/v04.001/reboot', require('./reboot'))
}
