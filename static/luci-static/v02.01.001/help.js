//help.js定义所有ID的形式，var table1 = {};var table2 = {};然后 feature.js传入table1或者table2，显示table还是table2
//定义各id如下数组格式，各功能需要显示的id数组，定义各ID内需要填入的信息

//routerInfo
var helpList = {
			//timeZone
			"timeZone":[
				[],
				[],
				["timeZone"]
			],
			//guideTimeZone
			"guideTimeZone":[
				["regionTimeZone"],
				["timeZone"],
				[]
			],
			//safeMng
			"safeMng":[
				[],
				[],
				["safeMng"]
			],
			//vpnServer
			"vpnServer":[
				[],
				[],
				["vpnServer","specialCharacter"]
			],
			//portForwad
			"portHelp":[
				[],
				[],
				["portFwdHelp"]
			],
			"portServerIp":[
				["portServerIp"],
				["portFwdIp"],
				[]
			],
			"portExternal":[
				["portExternal"],
				["portFwdexter"],
				[]
			],
			"portInternal":[
				["portInternal"],
				["portFwdinter"],
				[]
			],
			"portAgreeMent":[
				["portAgreeMent"],
				["portFwdAgerr"],
				[]
			],
			//igmp
			"igmp":[
				[],
				[],
				["igmpTip"]
			],
			//usbStorage
			"usbStorage":[
				[],
				[],
				["usbStorage","usbRemoveNote"]
			],
			//signalCondition
			"signalCondition":[
				[],
				[],
				["signalCondition"]
			],
			//backupReset
			"backupReset":[
				[],
				[],
				["backupReset","backupResetTip"]
			],
			//deviceManage
			"deviceMng":[
				[],
				[],
				["deviceMng"]
			],
			"forbidInternet":[
				["forbidInternet"],
				["forbidInternetTip"],
				[]
			],
			"rename":[
				["rename"],
				["renameTip"],
				[]
			],
			"speedLimit":[
				["speedLimit"],
				["speedLimitTip"],
				[]
			],
			//dhcpServer
			"dhcp":[
				["dhcp"],
				["dhcpTip"],
				[]
			],
			"addressPool":[
				["addressPool"],
				["addressPoolTip"],
				[]
			],
			"ipMacBind":[
				["ipMacBind"],
				["ipMacBindTip"],
				["ipMacBindTip1","ipMacBindTip2"]
			],
			//diagnose
			"diagnose":[
				[],
				[],
				["diagnose"]
			],
			//dmz
			"dmz":[
				[],
				[],
				["dmz","dmzTip"]
			],
			//light
			"light":[
				[],
				[],
				["light","lightTip"]
			],
			//Upnp
			"UPnP":[
				[],
				[],
				["upnp","upnpTip"]
			],
			//urlBlock
			"urlBlock":[
				[],
				[],
				["urlBlock","urlBlockTip","urlBlockTip1","urlBlockTip2"]
			],
			//woLink
			"woLink":[
				[],
				[],
				["wolinkTip","wolinkNote"]
			],
			//var vpnClient
			"vpnClient":[
				[],
				[],
				["vpnClient","specialCharacter"]
			],
			//ddns
			"ddns":[
				[],
				[],
				["ddns"]
			],
			"ddnsProvider":[
				["ddnsProvider"],
				["ddnsProviderOray"],
				["ddnsTip","networkTip"]
			],
			//screenSet
			"screenSet":[
				[],
				[],
				["screenSet"]
			],
			//qos
			"qos":[
				[],
				[],
				["qos"]
			],
			//modeSwitch
			"modeSet":[
				[],
				[],
				["modeSet"]
			],
			"routerMode":[
				["routerMode"],
				["routerModeTip"],
				[]
			],
			"apMode":[
				["apMode"],
				["apModeTip"],
				["modeSetNote"]
			],
			//elink
			"elink":[
				[],
				[],
				["elink"]
			],
			//iptv
			"iptv":[
				[],
				[],
				["iptv"]
			],
			//remoteMng
			"remoteMng":[
				[],
				[],
				["remoteMng","remoteMngTip"]
			],
			//whiteList
			"whiteList":[
				[],
				[],
				["whiteList","whiteListTip"]
			],
			//wifiSet
			"helpSmartConnect":[
				["smartConnect"],
				["smartConnectTip1"],
				[]
			],
			"wifiSetSsid":[
				["ssid"],
				["ssidTip"],
				[]
			],
			"wifiSetPwd":[
				["wifiPwd"],
				["wifiPwdTip"],
				[]
			],
			"wifiSetSsidHide":[
				["ssidHide"],
				["ssidHideTip"],
				[]
			],
			"wifiSetMode":[
				["wifiMode"],
				["wifiModeTip"],
				[]
			],
			"wifiSetChannel":[
				["channel"],
				["channelTip"],
				[]
			],
			"wifiSetband":[
				["bandWidth"],
				["bandWidthTip"],
				[]
			],
			"wifiSetApIsoalte":[
				["apIsolate"],
				["apIsolateTip"],
				[]
			],
			"helpWifiTimeSwitch":[
				["wifiTimeSwitch"],
				["wifiTimeSwitchTip"],
				[]
			],
			"helpMumimo":[
				["mumimo"],
				["mumimoTip"],
				[]
			],
			"helpBeamforming":[
				["beamforming"],
				["beamformingTip"],
				[]
			],
			"wifiSetTip":[
				[],
				[],
				["specialCharacter"]
			],
			//guideWifiSet 参数与wifiSet一样
			"wifiSetSsid":[
				["ssid"],
				["ssidTip"],
				[]
			],
			"wifiSetPwd":[
				["wifiPwd"],
				["wifiPwdTip"],
				[]
			],
			"wifiSetTip":[
				[],
				[],
				["specialCharacter"]
			],
			//lanset
			"lanSetIp":[
				["ip"],
				["ipTip"],
				[]
			],
			"netmask":[
				["netmask"],
				["netmaskTip"],
				[]
			],
			//manualUpgrade
			"manualUpgrade":[
				[],
				[],
				["manualUpgrade","manualUpgradeTip"]
			],
			//parentCtrl
			"parentCtrl":[
				[],
				[],
				["parentCtrlV2","parentCtrlV2Tip1","parentCtrlV2Tip"]
			],
			//setLgPwd
			"setLgPwd":[
				["adminPwd"],
				["adminPwdTip"],
				["specialCharacter"]
			],
			//signalSet
			"signalSet":[
				[],
				[],
				["signal"]
			],
			//sysUpgrade
			"sysUpgrade":[
				[],
				[],
				["autoUpgrade","autoUpgradeTip1","autoUpgradeTip2","autoUpgradeTip"]
			],
			//guest
			"guestSummary":[
				[],
				[],
				["guest"]
			],
			"helpGuestTimeSwitch":[
				["wifiTimeSwitch"],
				["guestTimeSwitchTip"],
				[]
			],
			"guestSsid":[
				["ssid"],
				["ssidTip"],
				[]
			],
			"guestPwd":[
				["wifiPwd"],
				["wifiPwdTip"],
				["ssidTip1"]
			],
			//wisp
			"wispSummary":[
				[],
				[],
				["wisp","wispTip","networkTip"]
			],
			//timeSleep
			"timeSleep":[
				[],
				[],
				["timeSleep","timeSleepTip1","timeSleepTip2"]
			]
	}
//

//处理feature.js中的ID，解析得到idList中value值
function dealHelpID(idList,idList1){
	var ret = "";
	var current_name = stateman.current.currentName;
	var name = current_name.replace("App","");
	for (key in idList1){
		if (key == name){
			ret += setHelpContent(idList1[key]);
			return ret;
		}
	}
	for (var key in idList){
		if (key == name){
			ret += setHelpContent(idList[key]);
			return ret;
		}
	}
	return ret;
}

//关闭帮助信息清空append内容
function clickHelp(){
	var current_name = stateman.current.currentName;
	var name = current_name.replace("App","");
	name = name.replace(name[0],name[0].toUpperCase());
	name = name.replace(name[0],"Help"+name[0])
	$("#"+name).append(dealHelpID(idList,helpIDList));
}
//定义格式要求如下，定义数组格式为二维数组，二维数组array[0]中内容填入title标array[1]中内容填入content内容中，array[2]中内容填入summary内容中。长度为0时，不操作。

function closeHelp(){
	var current_name = stateman.current.currentName;
	var name = current_name.replace("App","");
	name = name.replace(name[0],name[0].toUpperCase());
	$("#"+name).empty();
}

//处理各ID内的title，content内容，加载形成固定格式的字符串，返回字符串
function formHelpContent(idList){
	var idList1 = helpList[idList];
	var titlelength = idList1[0].length;
	var contentlength = idList1[1].length;
	var summarylength = idList1[2].length;
	var ret = "";
	if (titlelength > 0){
		for (var i = 0;i < titlelength;i++){
			ret += '<li class="title">' + helpStr[idList1[0][i]] + '</li>\n'
		}
	}

	if (contentlength > 0){
		for (var i = 0;i < contentlength;i++){
			ret += '<li class="content">' + helpStr[idList1[1][i]] + '</li>\n'
		}
	}

	if (summarylength > 0){
		for (var i = 0;i < summarylength;i++){
			ret += '<li class="summary">' + helpStr[idList1[2][i]] + '</li>\n'
		}
	}

	return ret;
}
//处理各功能内的各小id，形成一个总的包含<li conent title summary>的字符串
function setHelpContent(idList){
	var idlength = idList.length;
	var ret = "";
	for (var i = 0;i < idlength;i++){
		ret += formHelpContent(idList[i]);
	}
	return ret;
}
//处理feature.js传递的ID，覆盖之前的ID，或者增加新的ID。
function dealFeatureID(idList,idList1){
	for (var key in idList1){
			for (var key1 in idList){
				if (key1 == key){
					idList[key] = idList1[key1];
				}
			}
	}
}
