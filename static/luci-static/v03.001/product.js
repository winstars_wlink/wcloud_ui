function Product()
{
	this.LANGLIST = {
		langList: [],
		getLangList: function (){
			var langListAll = [{name:label.autoDetectLang, value:"auto"},
							{name:label.Chinese, value:"zh-cn"},
							{name:label.English, value:"en-us"},
							{name:label.German, value:"de-de"},
							{name:label.Russian, value:"ru-ru"},
							{name:label.French, value:"fr-fr"},
							{name:label.Spanish, value:"es-es"},
							{name:label.Italian, value:"it-it"},
							{name:label.Portuguese, value:"pt-pt"},
							{name:label.Ukrainian, value:"uk-ua"},
							{name:label.ChineseTraditional, value:"zh-tw"},
							{name:label.Japanese, value:"ja-jp"},
							{name:label.Korean, value:"ko-kr"},
							{name:label.Vietnamese, value:"vi-vi"},
							{name:label.Indonesian, value:"id-id"},
							{name:label.Thai, value:"th-th"}];

			if (this.langList.length <= 0){
				for (var i = 0; i < langListAll.length; i++){
					for (var j = 0; j < ROUTER.languageList.length; j++){
						if (ROUTER.languageList[j] == langListAll[i]["value"]){
							this.langList.push(langListAll[i]);
						}
					}
				}
			}
			return this.langList;
		}
	};

	this.TIMEZONE = {
		areaList: [],
		getRegionListAll: function () {
			// var regionListAll =  [{name:label.GMT01200, value:"01200"},
			// 			{name:label.GMT01100, value:"01100"},
			// 			{name:label.GMT01000, value:"01000"},
			// 			{name:label.GMT00900, value:"00900"},
			// 			{name:label.GMT00800, value:"00800"},
			// 			{name:label.GMT00700, value:"00700"},
			// 			{name:label.GMT00600, value:"00600"},
			// 			{name:label.GMT00500, value:"00500"},
			// 			{name:label.GMT00430, value:"00430"},
			// 			{name:label.GMT00400, value:"00400"},
			// 			{name:label.GMT00330, value:"00330"},
			// 			{name:label.GMT00300, value:"00300"},
			// 			{name:label.GMT00200, value:"00200"},
			// 			{name:label.GMT00100, value:"00100"},
			// 			{name:label.GMT00000, value:"00000"},
			// 			{name:label.GMT10100, value:"10100"},
			// 			{name:label.GMT10200, value:"10200"},
			// 			{name:label.GMT10300, value:"10300"},
			// 			{name:label.GMT10330, value:"10330"},
			// 			{name:label.GMT10400, value:"10400"},
			// 			{name:label.GMT10430, value:"10430"},
			// 			{name:label.GMT10500, value:"10500"},
			// 			{name:label.GMT10530, value:"10530"},
			// 			{name:label.GMT10545, value:"10545"},
			// 			{name:label.GMT10600, value:"10600"},
			// 			{name:label.GMT10630, value:"10630"},
			// 			{name:label.GMT10700, value:"10700"},
			// 			{name:label.GMT10800, value:"10800"},
			// 			{name:label.GMT10900, value:"10900"},
			// 			{name:label.GMT10930, value:"10930"},
			// 			{name:label.GMT11000, value:"11000"},
			// 			{name:label.GMT11100, value:"11100"},
			// 			{name:label.GMT11200, value:"11200"},
			// 			{name:label.GMT11300, value:"11300"}];
			var regionListAll =  [{name:label.UTC01200, value:"UTC+12:00"},
						{name:label.UTC01100, value:"UTC+11:00"},
						{name:label.UTC01000, value:"UTC+10:00"},
						{name:label.UTC00900, value:"UTC+09:00"},
						{name:label.UTC00800, value:"UTC+08:00"},
						{name:label.UTC00700, value:"UTC+07:00"},
						{name:label.UTC00600, value:"UTC+06:00"},
						{name:label.UTC00500, value:"UTC+05:00"},
						{name:label.UTC00430, value:"UTC+04:30"},
						{name:label.UTC00400, value:"UTC+04:00"},
						{name:label.UTC00330, value:"UTC+03:30"},
						{name:label.UTC00300, value:"UTC+03:00"},
						{name:label.UTC00200, value:"UTC+02:00"},
						{name:label.UTC00100, value:"UTC+01:00"},
						{name:label.UTC00000, value:"UTC+00:00"},
						{name:label.UTC10100, value:"UTC-01:00"},
						{name:label.UTC10200, value:"UTC-02:00"},
						{name:label.UTC10300, value:"UTC-03:00"},
						{name:label.UTC10400, value:"UTC-04:00"},
						{name:label.UTC10430, value:"UTC-04:30"},
						{name:label.UTC10500, value:"UTC-05:00"},
						{name:label.UTC10530, value:"UTC-05:30"},
						{name:label.UTC10545, value:"UTC-05:45"},
						{name:label.UTC10600, value:"UTC-06:00"},
						{name:label.UTC10630, value:"UTC-06:30"},
						{name:label.UTC10700, value:"UTC-07:00"},
						{name:label.UTC10800, value:"UTC-08:00"},
						{name:label.UTC10900, value:"UTC-09:00"},
						{name:label.UTC10930, value:"UTC-09:30"},
						{name:label.UTC11000, value:"UTC-10:00"},
						{name:label.UTC11100, value:"UTC-11:00"},
						{name:label.UTC11200, value:"UTC-12:00"},
						{name:label.UTC11300, value:"UTC-13:00"}]

			return regionListAll;
		},

		getAreaList: function () {
			var areaListAll = [{name:label.europe, value:"EU", sliceList:[{start: 14, end: 18}]},
								{name:label.northernAmerica, value:"NA", sliceList:[{start: 2, end: 14}]},
								{name:label.southAmerica, value:"SA", sliceList:[{start: 7, end: 12}]},
								{name:label.asia, value:"AS", sliceList:[{start: 17, end: 32}]},
								{name:label.africa, value:"AF", sliceList:[{start: 14, end: 20}]},
								{name:label.oceania, value:"OA", sliceList:[{start: 0, end: 2}, {start: 28, end: 34}]},
								{name:label.usRegion, value:"US", sliceList:[{start: 2, end: 8}]}];
			if (this.areaList.length <= 0){
				for (var i = 0; i < areaListAll.length; i++){
					for (var j = 0; j < ROUTER.areaList.length; j++){
						if (ROUTER.areaList[j] == areaListAll[i]["value"]){
							this.areaList.push(areaListAll[i]);
						}
					}
				}
			}

			return this.areaList;
		},
		getRegionList: function (area) {
			var regionList = [];
			var regionListAll = this.getRegionListAll();
			for(var i = 0; i < this.areaList.length; i++ ) {
				if(this.areaList[i].value == area){
					var tmpArea = this.areaList[i];
					var tmpRegion = [];
					for(var j = 0; j < tmpArea.sliceList.length; j++){
						tmpRegion = regionListAll.slice(tmpArea.sliceList[j].start, tmpArea.sliceList[j].end);
						regionList = regionList.concat(tmpRegion);
					}
				}
			}
			return regionList;
		}
	};
	this.TIMESCHEDULE = {
		getHourListAll: function () {
			var HourListAll =  [{name:"00", value:"00"},
							{name:"01", value:"01"},
							{name:"02", value:"02"},
							{name:"03", value:"03"},
							{name:"04", value:"04"},
							{name:"05", value:"05"},
							{name:"06", value:"06"},
							{name:"07", value:"07"},
							{name:"08", value:"08"},
							{name:"09", value:"09"},
							{name:"10", value:"10"},
							{name:"11", value:"11"},
							{name:"12", value:"12"},
							{name:"13", value:"13"},
							{name:"14", value:"14"},
							{name:"15", value:"15"},
							{name:"16", value:"16"},
							{name:"17", value:"17"},
							{name:"18", value:"18"},
							{name:"19", value:"19"},
							{name:"20", value:"20"},
							{name:"21", value:"21"},
							{name:"22", value:"22"},
							{name:"23", value:"23"}]
			return HourListAll;
		},
		getMinListAll: function () {
			var MinListAll =  [{name:"00", value:"00"},
							{name:"01", value:"01"},
							{name:"02", value:"02"},
							{name:"03", value:"03"},
							{name:"04", value:"04"},
							{name:"05", value:"05"},
							{name:"06", value:"06"},
							{name:"07", value:"07"},
							{name:"08", value:"08"},
							{name:"09", value:"09"},
							{name:"10", value:"10"},
							{name:"11", value:"11"},
							{name:"12", value:"12"},
							{name:"13", value:"13"},
							{name:"14", value:"14"},
							{name:"15", value:"15"},
							{name:"16", value:"16"},
							{name:"17", value:"17"},
							{name:"18", value:"18"},
							{name:"19", value:"19"},
							{name:"20", value:"20"},
							{name:"21", value:"21"},
							{name:"22", value:"22"},
							{name:"23", value:"23"},
							{name:"24", value:"24"},
							{name:"25", value:"25"},
							{name:"26", value:"26"},
							{name:"27", value:"27"},
							{name:"28", value:"28"},
							{name:"29", value:"29"},
							{name:"30", value:"30"},
							{name:"31", value:"31"},
							{name:"32", value:"32"},
							{name:"33", value:"33"},
							{name:"34", value:"34"},
							{name:"35", value:"35"},
							{name:"36", value:"36"},
							{name:"37", value:"37"},
							{name:"38", value:"38"},
							{name:"39", value:"39"},
							{name:"40", value:"40"},
							{name:"41", value:"41"},
							{name:"42", value:"42"},
							{name:"43", value:"43"},
							{name:"44", value:"44"},
							{name:"45", value:"45"},
							{name:"46", value:"46"},
							{name:"47", value:"47"},
							{name:"48", value:"48"},
							{name:"49", value:"49"},
							{name:"50", value:"50"},
							{name:"51", value:"51"},
							{name:"52", value:"52"},
							{name:"53", value:"53"},
							{name:"54", value:"54"},
							{name:"55", value:"55"},
							{name:"56", value:"56"},
							{name:"57", value:"57"},
							{name:"58", value:"58"},
							{name:"59", value:"59"}]
			return MinListAll;
		},
	};

	this.productSelect = function(listAll, listCur, callback) {
		var listNew = [];
		for (var i = 0; i < listAll.length; i++){
			for (var j = 0; j < listCur.length; j++){
				if (listCur[j] == listAll[i]["value"]){
					listNew.push(listAll[i]);
				}
			}
		}

		return listNew;
	}
	LANGLIST.getLangList();
	TIMEZONE.getAreaList();
	TIMESCHEDULE.getHourListAll();
	TIMESCHEDULE.MinListAll();
}

(function(){
	Product.call(window);
})();