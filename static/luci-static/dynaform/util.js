function Style()
{
	this.disableCol = "#b2b2b2";

	/* set the element styles with the styles */
	this.setStyle = function (ele, styles)
	{
		if (ele == null || styles == null || ele.nodeType != 1)
		{
			return;
		}

		for (var property in styles)
		{
			try
			{
				ele.style[property] = styles[property];
			}catch(ex){}
		}
	};

	/* get the default style of the element*/
	this.getNodeDefaultView = function(element, cssProperty)
	{
		var dv = null;
		if (!(element))
		{
			return null;
		}

		try{
			if (element.currentStyle)
			{
				dv = element.currentStyle;
			}
			else
			{
				dv = document.defaultView.getComputedStyle(element, null);
			}

			if (cssProperty != undefined)
			{
				return dv[cssProperty];
			}
			else
			{
				return dv;
			}
		}catch(ex){}
	};
}
function LocalStorageSD()
{
	try
	{
		if (null == this.sessionStorage)
		{
			this.sessionLS = {
				file_name:"user_data_default_SD",
				dom:null,
				init:function()
				{
					var dom = document.createElement('input');

					dom.type = "hidden";
					dom.addBehavior("#default#userData");
					document.body.appendChild(dom);
					dom.save(this.file_name);
					this.dom = dom;
				},
				setItem:function(k, v)
				{
					this.dom.setAttribute(k,v);
					this.dom.save(this.file_name);
				},
				getItem:function(k)
				{
					this.dom.load(this.file_name);
					return this.dom.getAttribute(k);
				},
				removeItem:function(k)
				{
					this.dom.removeAttribute(k);
					this.dom.save(this.file_name);
				},
				setExpire:function(timeSecond)
				{
				   var now = new Date();

				   now = new Date(now.getTime() + timeSecond);
				   this.dom.load(this.file_name);
				   this.dom.expires = now.toUTCString();
				   this.dom.save(this.file_name);
				}
			};
		}
		else
		{
			this.sessionLS = sessionStorage;
		}
	}catch(ex){};
}

function Tool()
{
	Style.call(this);

	/* get element by id */
	this.id = function(idStr)
	{
		if (idStr != undefined)
		{
			return document.getElementById(idStr);
		}
	};

	/* create element */
	this.el = function(str)
	{
		try
		{
			return document.createElement(str);
		}catch(ex){return null;}
	};

	/* replace {%....%} to realize multi languages */
	this.replaceJSP = function(str)
	{
		var matches = null, strRepace;
		var tagL = "{%", tagR = "%}";
		var rp = /{%(\w+)\.(\w+)%}/i;

		matches = rp.exec(str);
		try
		{
			while(matches != null)
			{
				strRepace = language[matches[1]][matches[2]];
				str = str.replace(tagL + matches[1] + "." + matches[2] + tagR, strRepace);
				matches = rp.exec(str);
			}
		}catch(ex){}
		return str;
	};
	this.getNearstInt = function (pix){
		var rt = pix;
		if (pix >= 1){
			rt = Math.floor(pix);
		} else if(pix > 0) {
			rt = 1;
		} else {
			rt = 0;
		}
		return rt;
	}
	this.replaceCSS = function(str){
		var tx = (window.screen.height / 900);

		return str.replace(/<style.*>[\s\S]+<\/style>/gmi, function(style){
			return style.replace(/\d+[\s]*px/gi, function(pix){
				var p = getNearstInt(parseInt(pix) * tx);
				return p + "px";
			})
		});
	};

	/* get the offsetLeft and offsetTop to the border of the container(default is browser) */
	this.getoffset = function(obj, container)
	{
		var tempObj = obj;
		var relPo = {
			top:0,
			left:0
		};

		while(true)
		{
			if (tempObj == container)
			{
				break;
			}

			relPo.left += parseInt(tempObj.offsetLeft);
			relPo.top += parseInt(tempObj.offsetTop);
			tempObj = tempObj.offsetParent;
		}

		return relPo;
	};

	this.attachEvnt = function(target, event, handle)
	{
		if (event.indexOf("on") == 0)
		{
			event = event.substring(2);
		}

		if (document.body.attachEvent)
		{
			target.attachEvent("on" + event, handle);
		}
		else
		{
			target.addEventListener(event, handle, false);
		}
	};

	this.detachEvnt = function(target, event, handle){
		if (event.indexOf("on") == 0)
		{
			event = event.substring(2);
		}

		if (document.body.attachEvent)
		{
			target.detachEvent("on" + event, handle);
		}
		else
		{
			target.removeEventListener(event, handle, false);
		}
	};

	/* stop propagation of event */
	this.stopProp = function (event)
	{
		event = event || window.event;
		if (event.stopPropagation)
		{
			event.stopPropagation();
		}
		else
		{
			event.cancelBubble = true;
		}
	};

	/* prevent defaut operation of event */
	this.eventPreventDefault = function (event)
	{
		event = event || window.event;
		if(event.preventDefault)
		{
			event.preventDefault();
		}
		else
		{
			event.returnValue = false;
		}
	};

	/* clear selection produced width mouse move */
	this.clearSelection = function ()
	{
		window.getSelection ? window.getSelection().removeAllRanges() : document.selection.empty();
	};

	/* 设置dom上range的光标位置 */
	this.setDomCursorPos = function (dom, pos)
	{
		if (dom.setSelectionRange)
		{
			dom.focus();
			dom.setSelectionRange(pos, pos);
		}
		else if (dom.createTextRange)
		{
			var range = dom.createTextRange()
			range.collapse(true);
			range.moveEnd('character', pos);
			range.moveStart('character', pos);
			range.select();
		}
	}

	/* get the pos of the mouse width the event */
	this.getMousePos = function (event)
	{
		event = event || window.event;
		var doc = document;
		var pos = (event.pageX || event.pageY) ? {x:event.pageX,y:event.pageY}:
				{x:event.clientX + doc.documentElement.scrollLeft - doc.documentElement.clientLeft,
				 y:event.clientY + doc.documentElement.scrollTop - doc.documentElement.clientTop};
		return pos;
	};

	/* 判断对象是否是数组 */
	this.isArray = function (obj)
	{
		return Object.prototype.toString.call(obj) === '[object Array]';
	};

	/* create up down */
	this.upDown = function (con, taId, classNameUp, classNameDown, callBack)
	{
		if (classNameUp == undefined || classNameDown == undefined)
		{
			return;
		}

		var lbl = this.el("label");

		lbl.className = classNameDown;
		lbl.onclick = function(){
			$("#"+taId).slideToggle("normal", function(){
				lbl.className = (lbl.className == classNameUp?classNameDown:classNameUp);
				if (callBack)
				{
					try
					{
						callBack();
					}catch(ex){}
				}
			});
		};
		con.appendChild(lbl);

		return lbl;
	};

	this.arrowUpDown = function (con, taId, callBack){
		this.upDown(con, taId, "arrowUp", "arrowDown", callBack);
	};

	/* 获取dom节点下指定类型的节点，index可选, filter:"input checkbox" */
	this.getChildNode = function(parent, filter, index){
		var childs = parent.childNodes;
		var nodes = [], count = 0, tempNode;
		var paras = filter.split(" ");
		var nodeName = paras[0], type = paras[1];

		for(var i = 0, len = childs.length;i < len; i++)
		{
			tempNode = childs[i];
			if (tempNode.nodeType == 1 && tempNode.tagName.toLowerCase() == nodeName)
			{
				if (type != undefined && tempNode["type"] == type)
				{
					nodes[count] = tempNode;
					count++;
				}
				else if (type == undefined)
				{
					nodes[count] = tempNode;
					count++;
				}
			}
		}
		if (index != undefined)
		{
			return nodes[index];
		}

		return nodes[0];
	};

	/* 检查节点是否可见 */
	this.checkInHorize = function(ta){
		var node = ta;
		while(node != null && node.nodeName.toUpperCase() != "HTML")
		{
			if (this.getNodeDefaultView(node, "visibility") == "hidden" ||
				this.getNodeDefaultView(node, "display") == "none")
			{
				return false;
			}
			node = node.parentNode;
		}

		return true;
	};

	this.setUrlHash = function(key, value)
	{
		var strH, strT, pos, tag ="";
		var url = location.href;
		var hash = location.hash;

		if (key == undefined ||
			value == undefined ||
			key.length == 0)
		{
			return;
		}

		if (hash.length != 0)
		{
			pos = hash.indexOf(key);
			if (pos >= 0)
			{
				strH = hash.substring(0, pos);
				strT = hash.substring(pos);
				pos = strT.indexOf("#");
				if (pos > 0)
				{
					strT = strT.substring(pos);
					hash = strH + key + "=" + value + strT;
				}
				else
				{
					hash = strH + key + "=" + value;
				}
			}
			else
			{
				if (hash.substring(hash.length - 1) != "#")
				{
					tag = "#";
				}
				hash += (tag + key + "=" + value);
			}

			location.href = url.substring(0, url.indexOf("#")) + hash;
		}
		else
		{
			if (url.lastIndexOf("#") == (url.length - 1))
			{
				location.href += (key + "=" + value);
			}
			else
			{
				location.href += ("#" + key + "=" + value);
			}
		}
	};

	this.getUrlHash = function(key)
	{
		var hash = location.hash;
		var pos = hash.indexOf(key);
		var strArr, tempArr, value = "";

		if (pos > 0)
		{
			strArr = hash.substring(1).split("#");
			for(var index in strArr)
			{
				tempArr = strArr[index].split("=");
				if (tempArr[0] == key)
				{
					value = tempArr[1];
					break;
				}
			}
		}

		return value;
	};

	this.changeUrlHash = function(str)
	{
		var url = location.href;
		var pos = url.indexOf("#");

		if (str == undefined)
		{
			return;
		}

		if (pos > 0)
		{
			location.href = url.substring(0, pos + 1) + str;
		}
		else
		{
			location.href = url + "#" +str;
		}
	};

	/* 设置输入框的光标的位置 */
	this.setInputCursor = function(input){
		var len = input.value.length;

		this.setDomCursorPos(input, len);
	};

	/* 获取字符串的长度，采用UTF-8编码，汉字占3个字符 */
	this.getCNStrLen = function(str)
	{
		return str.replace(/[^\x00-\xFF]/g, "xxx").length;
	};

	/* 截取字符串，最大长度为maxNum个字节 */
	this.getStrInMaxByte = function(str, maxNum){
		var bytes = 0;
		for (var i = 0; i < str.length; i++){
			if (/[\x00-\xFF]/g.test(str.charAt(i))){
				bytes += 1;
			}else{
				bytes += 3;		// utf-8编码的中文为3个字节
			}

			if (bytes > maxNum){
				return str.substring(0, i);
			}else if (bytes == maxNum){
				return str.substring(0, i+1);
			}
		}

		return str;
	};

	/* 获取字符串在HTML中的长度 */
	this.getCNStrHTMLLen = function(str){
		return str.replace(/[^\x00-\xFF]/g, "xx").length;
	};

	/* 截取字符串，如果超过maxNum则以...结束 */
	this.getStrInMax = function(value, maxNum){
		var str = "", strTemp, j = 0;
		var tmpStr = value.replace(/[A-Z]/g, "xx");

		if (getCNStrHTMLLen(tmpStr) <= maxNum)
		{
			return value;
		}

		for(var count = 1; count <= maxNum; count++)
		{
			strTemp = value.charAt(j);
			if (strTemp == "")
			{
				break;
			}

			if (getCNStrHTMLLen(strTemp) == 2 || /[A-Z]/g.test(strTemp) == true)
			{
				count++;
				str += strTemp;
				beCut = true;
			}
			else
			{
				str += strTemp;
			}

			j++;
		}
		return str + "...";
	};

	this.netSpeedTrans = function(speed){
		var kSpeed = 1024;
		var mSpeed = kSpeed * 1024;
		var gSpeed = mSpeed * 1024;

		speed = parseInt(speed, 10);
		if (speed >= gSpeed){
			speed = (speed/gSpeed).toFixed(2) + "GB/s";
		}
		else if (speed >= mSpeed){
			speed = (speed/mSpeed).toFixed(2) + "MB/s";
		}
		else if (speed >= kSpeed){
			speed = (speed/kSpeed).toFixed(0) + "KB/s";
		}
		else{
			speed = speed.toFixed(0) + "B/s";
		}

		return speed.toString();
	};

	this.timeTrans = function(total){
		var seconds = parseInt(total, 10);
		var day = parseInt(seconds/86400);
		var hour = parseInt((seconds%86400)/3600);
		var minute = parseInt((seconds%3600)/60);
		var second = parseInt(seconds%60);

		var timeStr = "";
		if (day > 0){
			timeStr += day + label.day + " ";
		}

		if (hour > 0){
			timeStr += hour + label.hour + " ";
		}

		if (minute > 0){
			timeStr += minute + label.minute + " ";
		}

		if (second >= 0){
			timeStr += second + label.second + " ";
		}

		return timeStr;
	};

	this.max = function(){
		var m = Number.NEGATIVE_INFINITY;
		for (var i = 0; i < arguments.length; i++)
		{
			if (arguments[i] > m) m = arguments[i];
		}

		return m;
	};

	this.EncodeURLIMG = document.createElement("img");

	/* 对多字节字符编码 */
	this.escapeDBC = function(s)
	{
		var img = this.EncodeURLIMG;

		if (!s)
		{
			return "";
		}

		if (window.ActiveXObject)
		{
			/* 如果是IE, 使用vbscript */
			execScript('SetLocale "zh-cn"', 'vbscript');
			return s.replace(/[\d\D]/g, function($0) {
				window.vbsval = "";
				execScript('window.vbsval=Hex(Asc("' + $0 + '"))', "vbscript");
				return "%" + window.vbsval.slice(0,2) + "%" + window.vbsval.slice(-2);
			});
		}

		/* 其它浏览器利用浏览器对请求地址自动编码的特性 */
		img.src = "nothing.png?separator=" + s;

		return img.src.split("?separator=").pop();
	};

	/* 对URL的参数进行GBK或UTF-8编码 */
	this.encodeURL = function(s)
	{
		return encodeURIComponent(s);

		/* 把 多字节字符 与 单字节字符 分开，分别使用 escapeDBC 和 encodeURIComponent 进行编码 */
		/*return s.replace(/([^\x00-\xff]+)|([\x00-\xff]+)/g, function($0, $1, $2) {
			return escapeDBC($1) + encodeURIComponent($2 || '');
		});*/
	};
	this.b64_to_utf8 = function(str)
	{
		return decodeURIComponent(escape(window.atob(str)));
	};
	this.utf8_to_b64 = function(str)
	{
		return window.btoa(unescape(encodeURIComponent(str)));
	};

	this.doNothing = function()
	{
		return true;
	};

	/* 转换特殊的HTML标记 */
	this.htmlEscape = function(str)
	{
		var escapseStr = str;

		if (undefined != escapseStr)
		{
			escapseStr = escapseStr.toString().replace(/[<>&"]/g, function(match){
				switch(match)
				{
				case "<":
					return "&lt;";
				case ">":
					return "&gt;";
				case "&":
					return "&amp;";
				case "\"":
					return "&quot;";
				}
			});
		}

		return escapseStr;
	};

	/* 模拟鼠标点击操作 */
	this.simulateMouseC = function (target)
	{
		if (true == isIE && false == isIENormal)
		{
			simulateMouseC = function(target){
				var event = document.createEventObject();

				event.sceenX = 100;
				event.sceenY = 0;
				event.clientX = 0;
				event.clientY = 0;
				event.ctrlKey = false;
				event.altKey = false;
				event.shiftKey = false;
				event.button = 0;

				target.fireEvent("onclick", event);
			};
		}
		else
		{
			simulateMouseC = function(){};
		}

		simulateMouseC(target);
	};
}


function ShowTips()
{
	this.showCover = function(){
		$("#Pop").show();
		$("#Cover").fadeIn("fast");
	};

	this.closeCover = function(){
		$("#Pop").hide();
		$("#Cover").fadeOut("fast");
	};
	/* 显示正在loading的状态 */
	this.showLoading = function(noteStr){
		var loadingCon =
					'<div class="loading">' +
						'<div class="outer-circle"><div class="inside-circle"><i class="circle"></i></div></div>' +
						'<p class="loading-tip">' + noteStr + '</p>' +
					'</div>';

		$("#Cover").fadeIn("fast");
		$("#Pop").empty().append(loadingCon).show();
	};

	/* 关闭正在loading的状态 */
	this.closeLoading = function(waitTime){
		if(waitTime) {
			window.setTimeout(function(){
					closeCover();
				}, waitTime*1000);
		} else {
			closeCover();
		}
	};
	this.handleError = function(errCode, callback, suffix){
		if(E_NONE == errCode)
			return false;
		var errTip = getErrTip(errCode, suffix);
		if(callback){
			callback(errTip);
		} else {
			showNote(errTip);
		}
		return true;
	};
	this.getErrTip = function(errCode, suffix){
		var err_code = "99999"
		if(parseInt(suffix)){
			err_code = "9" + errCode.toString().substring(2);
		} else {
			err_code = "" + errCode.toString().substring(1);
		}
		var errUnknow ="error_code_99999"
		var errNum = "error_code_"+(parseInt(err_code)).toString();
		var errTip = (errStrNew[errNum])?errStrNew[errNum]:(errStrNew[errUnknow]+errCode);
		return errTip;
	}
	/* 用于显示提示信息 */
	this.showNote = function(noteStr){
		$(".err-note").show();
		$(".err-note").children(".note-str").html(noteStr);
	};

	this.closeNote = function(){
		$(".err-note").hide();
	};

	this.showAlert = function(noteStr, callback){
		var loadingCon =
					'<div class="alert">' +
						'<p class="alert-title">' + label.alertTip + '</p>' +
						'<p class="alert-tip">' + noteStr + '</p>' +
						'<div class="alert-btn-con">'+
							'<input class="alert-btn" type="button" value="' + btn.ok + '" />' +
						'</div>'+
					'</div>';

		$("#Cover").fadeIn("fast");
		$("#Pop").empty().append(loadingCon).show();
		$("input.alert-btn").click(function(){
			closeAlert(callback);
		});
	};

	this.closeAlert = function(callback){
		closeCover();
		callback && callback();
	};

	this.showConfirm = function(noteStr, callback){
		var loadingCon =
					'<div class="alert">' +
						'<p class="alert-title">' + label.alertTip + '</p>' +
						'<p class="alert-tip">' + noteStr + '</p>' +
						'<div class="alert-btn-con">'+
							'<input class="confirm-btn confirm-btn-l" type="button" value="' + btn.cancel + '" />' +
							'<input class="confirm-btn confirm-btn-r" type="button" value="' + btn.ok + '" />' +
						'</div>'+
					'</div>';

		$("#Cover").fadeIn("fast");
		$("#Pop").empty().append(loadingCon).show();
		$("input.confirm-btn-l").click(function(){
			closeConfirm(function(){
				callback && callback(false);
			});
		});
		$("input.confirm-btn-r").click(function(){
			closeConfirm(function(){
				callback && callback(true);
			});
		});
	};

	this.closeConfirm = function(callback){
		closeCover();
		callback && callback();
	};

	this.progressBarTimer = null;
	this.refreshProgressBar = null;
	this.showProgressBar = function(title, tip, time, callback){
		var loadingCon =
					'<div class="alert progress-bar-con">' +
						'<p class="alert-title progress-title">' + title + '</p>' +
						'<div class="progress-bar">' +
							'<p class="percent"><label class="percent">0%</label></p>' +
							'<p class="percent"><i class="icon_position"></i></p>' +
							'<div class="bar"><p class="percent progress"></p></div>' +
						'</div>' +
						'<p class="progress-bar-tip">' + tip + '</p>' +
					'</div>';

		$("#Cover").fadeIn("fast");
		$("#Pop").empty().append(loadingCon).show();

		this.refreshBar = function(count){
			var percent = count + "%";
			var barWidth = $(".progress-bar").width();

			$("p.percent").width(percent);
			$("label.percent").text(percent);

			var textWidth = $("i.icon_position").width() / 2;
			var offsetText = barWidth * count / 100 - textWidth;
			if (offsetText > textWidth){
				offsetText = textWidth;
			}
			$("label.percent").css("left", offsetText + "px");

			var cursorWidth = $("i.icon_position").width() / 2;
			var offset = barWidth * count / 100 - cursorWidth;
			if (offset > cursorWidth){
				offset = cursorWidth;
			}

			$("i.icon_position").css("left", offset + "px");

			if (count >= 100){
				this.count = 0;
				clearInterval(progressBarTimer);
				callback && callback();
				return;
			}
		};

		this.progressBarPercent = 0;
		this.interval = time / 100;
		this.targetPercent = 100;

		this.countUpPercent = function(){
			this.progressBarPercent++;
			if(this.progressBarPercent > this.targetPercent)
				this.progressBarPercent--;
			return this.progressBarPercent;
		};

		this.refreshProgressBar = function(target, needTime){
			this.targetPercent = target;
			this.interval = needTime / (targetPercent - this.progressBarPercent);
			clearInterval(this.progressBarTimer);
			this.progressBarTimer = $.setInterval(function(){
				this.refreshBar(this.countUpPercent());
			}, this.interval);
		};

		this.refreshBar(this.countUpPercent());

		clearInterval(this.progressBarTimer);
		this.progressBarTimer = $.setInterval(function(){
			this.refreshBar(this.countUpPercent());
		}, this.interval);
	};
}


function Explorer()
{
	this.isIE = false;
	this.isIESix = false;
	this.isIESeven = false;
	this.isIENormal = false;
	this.isIETenLess = false;
	this.explorerInfo = navigator.userAgent;

	this.getIEInfo = function(){
		isIE = /msie ((\d+\.)+\d+)/i.test(explorerInfo)?(document.mode || RegExp["$1"]):false;

		if (isIE != false)
		{
			if (isIE <= 6)
			{
				this.isIESix = true;
			}
			else if (isIE == 7)
			{
				this.isIESeven = true;
			}
			else if (isIE >= 9)
			{
				this.isIENormal = true;
			}

			if (isIE <= 10)
			{
				this.isIETenLess = true;
			}

			this.isIE = true;
		}
	};

	this.compatibleShow = function(){
		if (isIE && false == this.isIENormal){
			var closeKey = "ieTipClosed";

			if (document.cookie.indexOf(closeKey) >= 0){
				return;
			}

			alert(label.IETip);
			document.cookie = closeKey + "=true";
		}
	};

	this.getIEInfo();
}

window.getCookie = function(name) {
  var match = document.cookie.match(new RegExp('(^| )' + name + '=([^;]+)'));
  if (match) return match[2];
}

window.eraseCookie = function(name) {   
  document.cookie = name+'=; Max-Age=-99999999;';  
}

window.waviot_get_time = function()
{
  var msgtime = new Date();
  msgtimestr =   msgtime.getFullYear() + 
                ("00" + (msgtime.getMonth() + 1)).slice(-2) +  
                ("00" + msgtime.getDate()).slice(-2) + 
                ("00" + msgtime.getHours()).slice(-2) +  
                ("00" + msgtime.getMinutes()).slice(-2) + 
                ("00" + msgtime.getSeconds()).slice(-2);

  var toffset = msgtime.getTimezoneOffset();
  if(toffset>=0)
      msgtimestr = msgtimestr + '-' + ("00" + toffset/60).slice(-2) + ("00" + toffset%60).slice(-2);
  else
      msgtimestr = msgtimestr + '+' + ("00" + (-toffset)/60).slice(-2) + ("00" + (-toffset)%60).slice(-2);
 
  return msgtimestr;
}

/* global variables */
var service_code = "WLINK_A0000"

window.initSwitch = function(switchId, state, callback){
	var thisObj = $("#" + switchId);
	state = state || 0;

	if (1 == state){
		thisObj.attr("data-value", "1").css({textAlign: "right", backgroundColor: "#3d65a3"});
		callback && callback(1, true);
	}else{
		thisObj.attr("data-value", "0").css({textAlign: "left", backgroundColor: "#B2B2B2"});
		callback && callback(0, true);
	}

	thisObj.click(function(){
		$(".err-note").hide();
		var val = $(this).attr("data-value");
		if (0 == val){
			$(this).attr("data-value", "1").css({textAlign: "right", backgroundColor: "#3d65a3"});
			callback && callback(1, false);
		}else{
			$(this).attr("data-value", "0").css({textAlign: "left", backgroundColor: "#B2B2B2"});
			callback && callback(0, false);
		}
	});
};
window.resetSwitch = function(switchId, state){
	var switchObj = $("#" + switchId);
	state = state || 0;

	if (1 == state){
		switchObj.attr("data-value", "1").css({textAlign: "right", backgroundColor: "#3d65a3"});
	}else{
		switchObj.attr("data-value", "0").css({textAlign: "left", backgroundColor: "#B2B2B2"});
	}
};
window.initRadio = function(id, checkedIdx, callback){
	var checked = "checked";
	var uncheck = "uncheck";
	var radios = $("#" + id + " span.radio");

	var idx = parseInt(checkedIdx, 10);
	var len = radios.length;
	for (var i = 0; i < len; i++){
		if (idx == i){
			$(radios[i]).removeClass(uncheck).addClass(checked).attr("data-idx", i);
		}else{
			$(radios[i]).removeClass(checked).addClass(uncheck).attr("data-idx", i);
		}
	}
	callback && callback(idx);

	radios.click(function(){
		var checked = "checked";
		var uncheck = "uncheck";
		var thisObj = $(this);
		var idx = parseInt(thisObj.attr("data-idx"));

		if (!thisObj.hasClass(checked)){
			thisObj.removeClass(uncheck).addClass(checked);
			thisObj.siblings("span.radio").removeClass(checked).addClass(uncheck);
			callback && callback(idx);
		}
	});
};

this.checkStr = function(value, checkFormat)
	{
		/* 十进制数字字符集 */
		if("decNumChar" == checkFormat)
		{
			if(/\D/g.test(value))
			{
				return false;
			}
		}
		/* 十六进制数字字符集 */
		if("hexNumChar" == checkFormat)
		{
			if(!(/^[0-9a-f]+$/gi.test(value)))
			{
				return false;
			}
		}
		/* 英文字符集（ASCII表示范围：0x20-0x7E） */
		if(undefined == checkFormat || "enChar" == checkFormat)
		{
			// var ch = "0123456789ABCDEFabcdefGHIJKLMNOPQRSTUVWXYZghijklmnopqrstuvwxyz`~!@#$^&*()-=_+[]{};:\'\"\\|/?.,<>/% ";
			var ch = "0123456789ABCDEFabcdefGHIJKLMNOPQRSTUVWXYZghijklmnopqrstuvwxyz!$%&*+-./:<=>?@^_`|}~";
			var chr;

			for (var i = 0, len = value.length; i < len; i++)
			{
				chr = value.charAt(i);
				if (ch.indexOf(chr) == -1){
					return false;
				}
			}
		}
		/* 数字字母字符集 */
		if("numAndLetters" == checkFormat)
		{
			if(!(/^[0-9a-z]+$/gi.test(value)))
			{
				return false;
			}

		}
		/* 英文字符集2（数字、字母、下划线） */
		if("enChar2" == checkFormat)
		{
			if(!(/^[0-9a-z_]+$/gi.test(value)))
			{
				return false;
			}
		}

		return true;
	};
	
this.checkStrFormat = function(value, checkFormat)
{
	if("string" != typeof(value))
	{
		return E_STRINGILLEGAL;
	}
	/* 字符串为空 */
	if("" == value)
	{
		return E_STRINGBLANK;
	}
	/* 检查字符串的字符集 */
	if (!(checkStr(value, checkFormat)))
	{
		return E_STRINGFORMAT;
	}

	return E_NONE;
};

this.checkStrLen = function(value, maxLen, minLen)
{
	var len = this.getCNStrLen(value);

	if (minLen > len || maxLen < len)
	{
		return E_STRINGLEN;
	}

	return E_NONE;
};


Tool.call(window);
LocalStorageSD.call(window);
ShowTips.call(window);
Explorer.call(window);

