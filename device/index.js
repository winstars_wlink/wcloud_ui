module.exports = (router) => {
  router.all('/', async (ctx) => {
    ctx.redirect('/error') ;
  });
  
  router.use('/networkSet', require('./networkSet'))
  router.use('/status', require('./status'))
  router.use('/app', require('./app'))
  router.use('/wifiset', require('./wifiset'))
  router.use('/error', require('./error'))
  router.use('/timeZone', require('./timeZone'))
  router.use('/deviceManage', require('./deviceManage'))
  router.use('/editClient', require('./editClient'))
  router.use('/modifyPwd', require('./modifyPwd'))
  router.use('/light', require('./light'))
  router.use('/signalCondition', require('./signalCondition'))
  router.use('/lanSet', require('./lanSet'))
  router.use('/guest', require('./guest'))
  router.use('/sysUpgrade', require('./sysUpgrade'))
  router.use('/meshManage', require('./meshManage'))
  router.use('/addMesh', require('./addMesh'))
  router.use('/touchLink', require('./touchLink'))
  router.use('/reboot', require('./reboot'))
}
