module.exports = {
  apps : [{
    name: 'wsock_router_ui',
    script: 'index.js',

    // Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file/
    args: '',
    autorestart: true,
    watch: false,
    instances: 0,
    max_memory_restart: '1G',
    env: {
      NODE_ENV: 'development',
      JWT_SECRET: "15567b09-88d2-45ba-9ddc-00314dde41a9",
    },
    env_localtest: {
      NODE_ENV: 'development',
      ONLINE_SESSION_TIME: 576,
      LOG_PATH: "logs",
      LOG_LEVEL: "debug",
      SESSION_TIMELIMIT: 15,
      PORT: 9088,
      BUILD_DATE : "202203301700",
      WCLOUD_SERVICE_CODE: "WLINK_A0000",
      WCLOUD_SERVICE_TAG : "ilovewinstarswavlinkwlink",
      JWT_SECRET : "15567b09-88d2-45ba-9ddc-00314dde41a9",
      SSL: 0,
      SSL_PATH: ""
    },
    env_development: {
      NODE_ENV: 'development',
      ONLINE_SESSION_TIME: 576,
      LOG_PATH: "logs",
      LOG_LEVEL: "debug",
      SESSION_TIMELIMIT: 15,
      PORT: 9088,
      BUILD_DATE : "202203301700",
      WCLOUD_SERVICE_CODE: "WLINK_A0000",
      WCLOUD_SERVICE_TAG : "ilovewinstarswavlinkwlink",
      JWT_SECRET : "15567b09-88d2-45ba-9ddc-00314dde41a9",
      SSL: 0,
      SSL_PATH: "/etc/letsencrypt/live/wsocktest.wavlink.org"
    },
    env_production: {
      NODE_ENV: 'production',
      ONLINE_SESSION_TIME: 576,
      LOG_PATH: "logs",
      LOG_LEVEL : "info",
      PORT: 4000,
      BUILD_DATE : "202203301700",
      WCLOUD_SERVICE_CODE: "WLINK_A0000",
      WCLOUD_SERVICE_TAG : "ilovewinstarswavlinkwlink",
      JWT_SECRET : "15567b09-88d2-45ba-9ddc-00314dde41a9",
      SSL: 0,
      SSL_PATH: "/etc/letsencrypt/live/wsock.wavlink.xyz"
    }
  }],
  deploy : {
    production : {
      user : 'ubuntu',
      host : ['uicn1.wsock.wavlink.xyz', 'uihk1.wsock.wavlink.xyz' ],
      ref  : 'origin/master',
      repo : 'git@bitbucket.org:winstars_wlink/wcloud_ui.git',
      path : '/home/ubuntu/service/wlink/wcloud_router_ui',
      ssh_options: ['ForwardAgent=yes'],
      'post-deploy' : 'git submodule update --init --recursive && yarn && pm2 reload ecosystem.config.js --env production'      
    },
    development: {
      user : 'ubuntu',
      host : '3.113.247.35',
      ref  : 'origin/master',
      repo : 'git@bitbucket.org:winstars_wlink/wcloud_ui.git',
      path : '/home/ubuntu/service/wlink/ui_demo',
      ssh_options: ['ForwardAgent=yes', "PasswordAuthentication=no"],
      'post-deploy' : 'git submodule update --init --recursive && yarn && sudo pm2 reload ecosystem.config.js --env development'
    },
    localtest: {
      user : 'wcloudtest',
      host : '192.168.3.18',
      ref  : 'origin/master',
      repo : 'git@192.168.3.18:wcloud/ui_demo.git',
      path : '/home2/zhongyanmin/wcloud_ui',
      ssh_options: ['ForwardAgent=yes', "PasswordAuthentication=no"],
      'post-deploy' : 'git submodule update --init --recursive && yarn && pm2 reload ecosystem.config.js --env localtest'
    },    
    dev: {}    
  }
};
