require('dotenv').config();
const path = require('path');
var fs = require('fs');
var https  = require('https');

// env to config
var config = 
{
  node_env : process.env.NODE_ENV || "development",
  port :  process.env.PORT || 9088,
  use_ssl : process.env.SSL || 0,
  ssl_path : process.env.SSL_PATH || "",
  log_path : process.env.LOG_PATH || "logs",
  log_level : process.env.LOG_LEVEL || "debug",
  db : process.env.DB,
  wcloud_service_code: process.env.WCLOUD_SERVICE_CODE,
  wcloud_service_tag: process.env.WCLOUD_SERVICE_TAG,
  jwt_sceret : process.env.JWT_SECRET,
  session_time : parseInt(process.env.ONLINE_SESSION_TIME) || 2,
  session_timelimit : parseInt(process.env.SESSION_TIMELIMIT) || 15,
  path : path.join(__dirname),
  login_token_timeLimit : parseInt(process.env.LOGINTOKEN_TIMELIMIT) || 30, // 30 days
  random_key_timeLimit : parseInt(process.env.RANDOMKEY_TIMELIMIT) || 5,  //5 mins
  file_session_timeLimit : parseInt(process.env.FILE_SESSION_TIMELIMIT) || 5,
  isProduction : (cfg) => cfg.node_env!=="development",
  build_date : process.env.BUILD_DATE,
}

global.config = config;
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
// init log
var Logger = require('./lib/logger');
var logger = new Logger('wcloud_ui');
logger.level = config.log_level;
global.logger = logger;

const server = require('./server');
if(config.use_ssl==0)
{
  server.listen(config.port, () => console.log(`UI server started on ${config.port}`))
}
else
{
  var ssl_options = {
    key: fs.readFileSync(config.ssl_path+'/privkey.pem'),
    cert: fs.readFileSync(config.ssl_path+'/fullchain.pem'),
    requestCert: false,
    rejectUnauthorized: false    
  };

  var https_service = https.createServer(ssl_options, server.callback());
  https_service.listen(config.port, () => console.log(`UI(HTTPS) server started on ${config.port}`))
}
