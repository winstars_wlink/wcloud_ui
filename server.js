const Koa = require('koa');
const session = require('koa-session');
const Router = require('koa-router');
const Logger = require('koa-logger');
const static = require("koa-static");
const staticCache = require('koa-static-cache');
const Cors = require('@koa/cors');
const BodyParser = require('koa-bodyparser')
const Helmet = require('koa-helmet');
const respond = require('koa-respond')
const render = require('koa-ejs');
const path = require('path');
const locales = require('koa-locales');
const config = global.config;
const app = new Koa()
const compress = require('koa-compress')

app.use(BodyParser());
app.keys = ['f73c569e-d221-4ca9-b6bb-fbfbe9f63f50'];

const CONFIG = {
  key: 'wavlink"f73c569e-d221-4ca9-b6bb-fbfbe9f63f50', /** (string) cookie key (default is koa:sess) */
  /** (number || 'session') maxAge in ms (default is 1 days) */
  /** 'session' will result in a cookie that expires when session/browser is closed */
  /** Warning: If a session cookie is stolen, this cookie will never expire */
  maxAge: 86400000,
  autoCommit: true, /** (boolean) automatically commit headers (default true) */
  overwrite: true, /** (boolean) can overwrite or not (default true) */
  httpOnly: true, /** (boolean) httpOnly or not (default true) */
  signed: true, /** (boolean) signed or not (default true) */
  rolling: false, /** (boolean) Force a session identifier cookie to be set on every response. The expiration is reset to the original maxAge, resetting the expiration countdown. (default is false) */
  renew: false, /** (boolean) renew session when session is nearly expired, so we can always keep user logged in. (default is false)*/
};

app.use(session(CONFIG, app));

if (process.env.NODE_ENV != 'production') 
{
  console.log("use local static path...")
  app.use(compress({
    filter: function (content_type) {
      return /text/i.test(content_type)
    },
    threshold: 2048,
    flush: require('zlib').Z_SYNC_FLUSH
  }))
  app.use(staticCache(__dirname + '/static', {
   maxAge: 24 * 60 * 60  //Add these files to caches for a year
 }))
}

const options = {
  dirs: [__dirname + '/static/luci-static/locales',
          __dirname + '/static/luci-static/locales_v02.001',
          __dirname + '/static/luci-static/locales_v02.002',
          __dirname + '/static/luci-static/locales_v03.001',
          __dirname + '/static/luci-static/locales_v04.001',
          __dirname + '/static/luci-static/locales_v02.01.001',
          __dirname + '/static/luci-static/locales_v02.03.001'],
  defaultLocale: 'en-us',
};
locales(app, options);

app.use(Helmet())

if (process.env.NODE_ENV === 'development') {
  app.use(Logger())
}

app.proxy = true;
app.use(Cors())

render(app, {
  root: path.join(__dirname, 'router'),
  layout: "layout/layout",
  viewExt: 'html',
  cache: false,
  debug: false
});

var deviceRouter = new Router();
require('./device' )(deviceRouter)
app.use(deviceRouter.routes())
app.use(deviceRouter.allowedMethods())

var deviceRouter_v02001 = new Router();
require('./device_v02.001' )(deviceRouter_v02001)
app.use(deviceRouter_v02001.routes())
app.use(deviceRouter_v02001.allowedMethods())

var deviceRouter_v02002 = new Router();
require('./device_v02.002' )(deviceRouter_v02002)
app.use(deviceRouter_v02002.routes())
app.use(deviceRouter_v02002.allowedMethods())

var deviceRouter_v03001 = new Router();
require('./device_v03.001' )(deviceRouter_v03001)
app.use(deviceRouter_v03001.routes())
app.use(deviceRouter_v03001.allowedMethods())

var deviceRouter_v04001 = new Router();
require('./device_v04.001' )(deviceRouter_v04001)
app.use(deviceRouter_v04001.routes())
app.use(deviceRouter_v04001.allowedMethods())

var deviceRouter_v0201001 = new Router();
require('./device_v02.01.001' )(deviceRouter_v0201001)
app.use(deviceRouter_v0201001.routes())
app.use(deviceRouter_v0201001.allowedMethods())
app.use(respond())

var deviceRouter_v0203001 = new Router();
require('./device_v02.03.001' )(deviceRouter_v0203001)
app.use(deviceRouter_v0203001.routes())
app.use(deviceRouter_v0203001.allowedMethods())
app.use(respond())

module.exports = app
