module.exports = (router) => {
  router.all('/', async (ctx) => {
    ctx.redirect('/error') ;
  });
  
  router.use('/v03.001/networkSet', require('./networkSet'))
  router.use('/v03.001/status', require('./status'))
  router.use('/v03.001/app', require('./app'))
  router.use('/v03.001/wifiset', require('./wifiset'))
  router.use('/v03.001/error', require('./error'))
  router.use('/v03.001/timeZone', require('./timeZone'))
  router.use('/v03.001/deviceManage', require('./deviceManage'))
  router.use('/v03.001/editClient', require('./editClient'))
  router.use('/v03.001/modifyPwd', require('./modifyPwd'))
  router.use('/v03.001/light', require('./light'))
  router.use('/v03.001/signalCondition', require('./signalCondition'))
  router.use('/v03.001/lanSet', require('./lanSet'))
  router.use('/v03.001/guest', require('./guest'))
  router.use('/v03.001/sysUpgrade', require('./sysUpgrade'))
  router.use('/v03.001/meshManage', require('./meshManage'))
  router.use('/v03.001/addMesh', require('./addMesh'))
  router.use('/v03.001/touchLink', require('./touchLink'))
  router.use('/v03.001/reboot', require('./reboot'))
}
