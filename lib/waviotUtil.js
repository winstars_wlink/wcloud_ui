var moment = require('moment-timezone');
var crypto = require('crypto');
var uuid = require('node-uuid');
var random_seed = require('random-seed');
var AWS = require("aws-sdk");
const random_key_timeLimit = global.config.random_key_timeLimit; // 5 minuts
const login_token_timeLimit = global.config.login_token_timeLimit; // 30 days
const file_session_timeLimit = global.config.file_session_timeLimit;

function get_session_auth(msgtime, service_code, service_tag, uuid, apikey, secret)
{
    var plain = msgtime+service_code+service_tag+uuid+apikey;
    var hmac = crypto.createHmac("md5", secret);
    hmac.update(plain);

    return hmac.digest("hex");
}

// today - expired
// over 0  : expired 
function check_service_expired(expired_date)
{
    var today = moment();
    var expired = moment(expired_date);
    return  today.diff(expired, 'days');
}

// today - expired
// under 0  : remain time 
function check_session_expired(session_expired)
{
    var today = moment();    
    var expired = moment(session_expired);
    return  today.diff(expired, 'minutes');
}

function check_random_key_expired(random_key_expire)
{
    var today = moment();
    var expired = moment(random_key_expire);
    var diff = expired.diff(today, 'minute');

    // time is passed
    if(diff<0)
        return true;
        
    // time has problem
    if(diff>random_key_timeLimit)
        return true;

    return false;
    
}


// true : expired  즉 로그인 가능하다.
// false : not expired.  즉 로그인 불가
function check_login_fail_expired(login_fail_expire)
{
    var today = moment();
    var expired = moment(login_fail_expire);
    var diff = expired.diff(today, 'minute');

    // time is passed
    if(diff<0)
        return true;
    return false;
}

// true : expired
// false : not expired. it is valid.
function check_login_token_expired(login_token_expired)
{
    var today = moment();
    var expired = moment(login_token_expired);
    var diff = expired.diff(today, 'days');

    // time is passed
    if(diff<0)
        return true;

    // time has problem
    if(diff>login_token_timeLimit)
        return true;

    return false;
}

// today - expired
// over 0  : expired 
function check_msg_expired(msgtime, limit_time)
{
    var _msgtime = moment(msgtime, "YYYYMMDDHHmmssZZ");    
    var expired = moment();
    expired.add(-limit_time, "minute");

    var diff =  expired.diff(_msgtime, 'minutes');

    //console.log(expired.format('YYYYMMDDHHmmssZZ')+" DIFF: " + diff);

    if(diff<(-limit_time))
        return -diff;

    else 
        return diff;
}

function _s4(seed_uuid) {
    var rand = random_seed.create(seed_uuid);
    return ((1 +  rand.random()+Math.random()) * 0x10000 | 0).toString(16).substring(1);
}

function gen_session_key(seed_uuid) {
    return _s4(seed_uuid) + _s4(seed_uuid) + _s4(seed_uuid) + _s4(seed_uuid) + _s4(seed_uuid) +
                _s4(seed_uuid) + _s4(seed_uuid) + _s4(seed_uuid);
}

function gen_api_key(service_code, seed_uuid)
{
  return service_code+"_"+_s4(seed_uuid) + _s4(seed_uuid) + _s4(seed_uuid) + _s4(seed_uuid) + _s4(seed_uuid) + _s4(seed_uuid);
}

function gen_api_secret(seed_uuid) {
  return _s4(seed_uuid) + _s4(seed_uuid) + _s4(seed_uuid) + _s4(seed_uuid) + _s4(seed_uuid) +
              _s4(seed_uuid) + _s4(seed_uuid) + _s4(seed_uuid) + _s4(seed_uuid) + _s4(seed_uuid);
}

function get_ext_auth(msgtime, apikey, service_code, ext_name, command, secret)
{
    var plain = msgtime+apikey+service_code+ext_name+command;
    var hmac = crypto.createHmac("md5", secret);
    hmac.update(plain);

    return hmac.digest("hex");
}

function get_users_auth(msgtime, apikey, service_code, command, account_type, account_id, secret)
{
    var plain = msgtime+apikey+ service_code+command+account_type+account_id;
    var hmac = crypto.createHmac("md5", secret);
    hmac.update(plain);

    return hmac.digest("hex");
}

// true : expired
// false : not expired. it is valid.
function check_file_session_key_expired(file_session_key_expire)
{
    var today = moment();
    var expired = moment(file_session_key_expire);
    var diff = expired.diff(today, 'minute');

    // time is passed
    if(diff<0)
        return true;
        
    // time has problem
    if(diff>file_session_timeLimit)
        return true;

    return false;
 
}

function gen_file_session_key_time()
{
    var file_session_key_time  = moment();
    file_session_key_time.add(file_session_timeLimit, "minute");
    
    return file_session_key_time;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
const verification_session_timeLimit = 10;
// true : expired
// false : not expired. it is valid.
function check_verification_session_key_expired(verification_session_expired)
{
    var today = moment();
    var expired = moment(verification_session_expired);
    var diff = expired.diff(today, 'minute');

    // time is passed
    if(diff<0)
        return true;
        
    // time has problem
    if(diff>verification_session_timeLimit)
        return true;

    return false;
}

////////////////////////////////////////////////////////////////////////////////////////////////
const check_verification_code_limit_timeout = 2;
// true : not over call limt time. 
// false : over the time limt. it is valid.
function check_verification_code_timelimit(verification_code_time)
{
    var today = moment();
    var expired = moment(verification_code_time);
    var diff = today.diff(expired, 'minute');

    console.log("check_verification_code_timelimit:"+diff)
    // time is passed
    if(diff<check_verification_code_limit_timeout)
        return true;

    return false;
}

function gen_verification_session_key_time()
{
    var verification_session_key_time  = moment();
    verification_session_key_time.add(verification_session_timeLimit, "minute");
    
    return verification_session_key_time;
}

function gen_verification_code()
{
    var today = moment();
    var seed_uuid = today.format("YYYYMMDDHHmmssZZ").toString();;
    var rand = random_seed.create(seed_uuid);
    return ((1 +  rand.random()+Math.random()) * 100000000 | 0).toString(10).substring(1, 7);
}

function make_koa_response_msg(ctx, code, msg)
{
  ctx.status = code;
  ctx.body = {
    message: msg
  }
}
////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
function init_global_service()
{
  let i;
  global.service = new Map();

  for(i=0; i<global.config.io_service.length; i++)
  {
    let config = {
      service_code : global.config.io_service[i].service_code,
      baidu : global.config.io_service[i].baidu, 
      getui : global.config.io_service[i].getui, 
      apn_debug : global.config.io_service[i].apn_debug, 
      apn_release : global.config.io_service[i].apn_release, 
      apn_topic : global.config.io_service[i].apn_topic, 
      aws : global.config.io_service[i].aws,
      smtp: global.config.io_service[i].smtp
    };
    config.aws_config = new AWS.Config(global.config.io_service[i].aws);
    global.service.set(global.config.io_service[i].service_code, config);
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////
function get_global_service(service_code)
{
    return global.service.get(service_code);
}


var waviotUtil = {
    gen_session_key,
    gen_api_key,
    gen_api_secret,
    get_session_auth,
    check_service_expired,
    check_session_expired,
    check_msg_expired,
    check_login_token_expired,
    check_login_fail_expired,
    check_random_key_expired,
    get_ext_auth,
    get_users_auth,
    check_file_session_key_expired,
    gen_file_session_key_time,
    init_global_service,
    get_global_service,
    gen_verification_code,
    check_verification_code_timelimit,
    check_verification_session_key_expired,
    gen_verification_session_key_time,
    make_koa_response_msg
};

module.exports = waviotUtil;
